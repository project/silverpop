<?php

/**
 * @file
 * Module file for the silverpop_webform module.
 */

/**
 * Implements hook_menu().
 */
function silverpop_webform_menu() {
  $items['admin/config/services/silverpop/webform'] = array(
    'title' => 'Silverpop enabled WebForms',
    'description' => 'Webforms which have been enabled for silverpop_webform.',
    'page callback' => 'silverpop_webform_enabled_silverpop_webform_load',
    'access arguments' => array('administer silverpop settings'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 20,
  );
  $items['node/%webform_menu/webform/silverpop_webform'] = array(
    'title' => 'Silverpop',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('silverpop_webform_components_form', 1),
    'access callback' => 'node_access',
    'access arguments' => array('update', 1),
    'file' => 'silverpop_webform.admin.inc',
    'weight' => 1,
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Implements hook_form_alter().
 */
function silverpop_webform_form_alter(&$form, $form_state, $form_id) {
  if (strpos($form_id, 'webform_client_form') !== 0) {
    return;
  }
  // Modify all webforms to use this output thingy.
  // Load the form specific settings.
  $nid = $form['details']['nid']['#value'];
  if (empty($nid)) {
    return;
  }
  $form_settings = silverpop_webform_load($nid);

  // Check to see if the form exists.
  if (empty($form_settings) || (!$form_settings->is_active)) {
    return;
  }

  $form['#submit'] = is_array($form['#submit']) ? $form['#submit'] : array('webform_client_form_submit ');
  $form['#submit'][] = 'silverpop_webform_submit';
}

/**
 * Submit handler added to webforms to store submissions for silverpop_webform.
 *
 * @param array $form
 * @param array $form_state
 */
function silverpop_webform_submit($form, &$form_state) {

  // Collapse form values so they make more sense to silverpop_webform.
  $values = _silverpop_webform_form_collapse($form, $form_state);

  $record = silverpop_webform_load($form['#node']->nid);
  if (empty($record->add_to_contact_list)) {
    $record->list_id = NULL;
  }

  $recipient = array();

  // Build recipient record
  foreach ($record->data as $field_name => $mapping) {
    if (array_key_exists($field_name, $values)) {
      $recipient[$mapping['key']] = $values[$field_name];
    }
  }

  // Later versions of webform key by cid.
  $node = node_load($form_state['values']['details']['nid']);
  foreach ($values as $cid => $value) {
    if (is_numeric($cid) && isset($node->webform['components'][$cid])) {
      $component = $node->webform['components'][$cid];
      if (array_key_exists($component['form_key'], $record->data)) {
        $mapping = $record->data[$component['form_key']];
        $recipient[$mapping['key']] = $value;
      }
    }
  }

  // Include Silverpop API
  module_load_include('inc', 'silverpop', 'silverpop.api');

  // Use values from webform to add a Silverpop recipient.
  if ($recipient) {
    silverpop_addrecipient($record->database_id, $record->list_id, $recipient);
  }
}

/**
 * Load a silverpop_webform form settings object.
 *
 * @param integer $nid
 *   Associated webform node id.
 * @return
 *   Settings object or false on failure.
 */
function silverpop_webform_load($nid) {
  $record = db_query("SELECT * FROM {silverpop_webform} WHERE nid = :nid", array(':nid' => $nid))->fetchObject();
  if ($record) {
    $record->data = unserialize($record->data);
  }

  return $record;
}

/**
 * Collapses a submitted form into a flat array for silverpop_webform.
 *
 * @param array $form
 * @param array $form_state
 */
function _silverpop_webform_form_collapse($form, $form_state) {
  $result = array();

  $form_tree = !empty($form_state['values']['submitted_tree']) ? $form_state['values']['submitted_tree'] : $form_state['values']['submitted'];

  _silverpop_webform_form_collapse_form($form_tree, $form_state['values']['submitted'], $form['submitted'], $result);

  return $result;
}

/**
 * Helper function to recurse through posted webform.
 *
 * @see _silverpop_webform_form_collapse()
 * @param $tree
 *   The post tree name => value pairs
 * @param $posted_values
 *   The post tree, could be name => value pairs or index => value pairs.
 * @param $form
 *   The actual form structure of the form.
 * @param $result
 *   Return by reference re-structured tree that silverpop_webform will leverage
 * @return none
 */
function _silverpop_webform_form_collapse_form($tree, $posted_values, $form, &$result) {
  foreach ($tree as $name => $value) {
    // we need to expand things in fieldsets
    if (is_array($value) && !in_array($value, $posted_values)) {
      _silverpop_webform_form_collapse_form($value, $posted_values, $form[$name], $result);
    }
    // we need to convert multi-value fields into strings
    elseif (is_array($value)) {
      // If it looks like a date, and the year is reasonable then use slashes
      // 0-1-2 = M/D/Y
      if ((count($value) == 3)
        && ($value[2] > 1900) && ($value[2] < 2100)
        && (checkdate($value[0], $value[1], $value[2]))) {
        $result[$name] = implode('/', $value);
      }
      else {
        $result[$name] = implode(',', $value);
      }
    }
    // everything else is just passed along
    elseif (isset($form[$name]['#type']) && ($form[$name]['#type'] == 'select')) {
      // Map select values to text versions. The numeric values won't mean
      // much to silverpop_webform, CRM or any other integration.
      $result[$name] = $form[$name]['#options'][$value];
    }
    else {
      $result[$name] = $value;
    }
  }
}

/**
 * Load all webform enabled nodes with silverpop_webform post url
 *
 * @return string
 *   Settings object or false on failure.
 */
function silverpop_webform_enabled_silverpop_webform_load() {
  $results = db_query("SELECT * FROM {silverpop_webform}");
  $header = array(t('Node'), t('Edit Settings'), t('Database ID'), t('Contact List ID'), t('Active'));
  $rows = array();

  if ($results->rowCount() > 0) {
    foreach ($results as $result) {
      $node = node_load($result->nid);
      $rows[] = array(
        l($node->title, "node/{$result->nid}"),
        l(t('Edit'), "node/{$result->nid}/webform/silverpop_webform"),
        $result->database_id,
        empty($result->add_to_contact_list) ? t('N/a') : $result->list_id,
        $result->is_active,
      );
    }

    $output = theme('table', array('header' => $header, 'rows' => $rows));
  }
  else {
    $output = '<p>No Silverpop webforms have been enabled.</p>';
  }

  return $output;
}

/**
 * Implements hook_node_delete().
 *
 * Removes silverpop_webform record when its node is deleted.
 */
function silverpop_webform_node_delete($node) {
  if (!variable_get('webform_node_' . $node->type, FALSE)) {
    return;
  }

  db_delete('silverpop_webform')->condition('nid', $node->nid)->execute();
}

/**
 * Implements hook_theme().
 */
function silverpop_webform_theme($existing, $type, $theme, $path) {
  return array(
    'silverpop_webform_components_form' => array(
      'render element' => 'form',
      'file' => 'silverpop_webform.admin.inc',
    ),
  );
}

/*
 * Implements hook_webform_component_delete($component).
 *
 * Updates silverpop_webform mappings when component is deleted.
 */
function silverpop_webform_webform_component_delete($component) {

  $record = silverpop_webform_load($component['nid']);
  unset($record->data[$component['form_key']]);

  $update = array('nid');
  drupal_write_record('silverpop_webform', $record, $update);

  drupal_set_message(t('Silverpop webform configuration has been updated.'));
}